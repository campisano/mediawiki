#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

if test -f /tmp/build.date
then
    echo -en "Build at "
    cat /tmp/build.date
    echo
fi



if test -z "${FRESH_INSTALL:-}" -o "${FRESH_INSTALL:-}" != "true"
then
    # Settings
    cat mediawiki/LocalSettings.php.template | sed 's/\$/§/g' | sed 's/§{/\${/g' | envsubst | sed 's/§/\$/g' > mediawiki/LocalSettings.php
    chmod 644 mediawiki/LocalSettings.php

    # sitemap.xml
    php mediawiki/maintenance/run.php generateSitemap.php --conf=mediawiki/LocalSettings.php --memory-limit=50M --fspath=tmp --identifier="${MW_SITE_NAME}" --server="${MW_SERVER}" --compress=no --skip-redirects
    mv "tmp/sitemap-${MW_SITE_NAME}-NS_0-0.xml" sitemap.xml
    rm -rf tmp
    chmod 644 sitemap.xml
fi

rm -f mediawiki/LocalSettings.php.template



# robots.txt
cat robots.txt.template | envsubst > robots.txt
chmod 644 robots.txt
rm -f robots.txt.template



exec "$@"
