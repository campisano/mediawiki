#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

IMAGE="docker.io/mediawiki:1.41.0"

echo "${IMAGE}"
